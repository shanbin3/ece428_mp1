package main

import (
	"bufio"
	"container/heap"
	"fmt"
	"math"
	"net"
	"os"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
)

type Item struct {
	Content  interface{}
	Priority float64
	Final    bool
	NodeName string
	Cnt      int
	Index    int
}

type PriorityQueue []*Item

func NewPriorityQueue() *PriorityQueue {
	pq := make(PriorityQueue, 0)
	heap.Init(&pq)
	return &pq
}

func (pq PriorityQueue) Len() int { return len(pq) }

func (pq PriorityQueue) Less(i, j int) bool {
	return pq[i].Priority < pq[j].Priority
}

func (pq PriorityQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
	pq[i].Index = i
	pq[j].Index = j
}

func (pq *PriorityQueue) Push(x interface{}) {
	item := x.(*Item)
	item.Index = len(*pq)
	*pq = append(*pq, item)
}

func (pq *PriorityQueue) Pop() interface{} {
	old := *pq
	n := len(old)
	item := old[n-1]
	item.Index = -1
	*pq = old[0 : n-1]
	return item
}

type PriorityQueueWithMutex struct {
	pq        *PriorityQueue
	maxPrio   float64
	maxMsgCnt int
	mutex     sync.Mutex
}

func NewPriorityQueueWithMutex() *PriorityQueueWithMutex {
	pq := NewPriorityQueue()
	return &PriorityQueueWithMutex{pq: pq}
}

func (pq *PriorityQueueWithMutex) Enqueue(item *Item) {
	pq.mutex.Lock()
	defer pq.mutex.Unlock()

	heap.Push(pq.pq, item)
	if item.Priority > pq.maxPrio {
		pq.maxPrio = item.Priority
	}
}
func (pq *PriorityQueueWithMutex) Enqueue_unlock(item *Item) {

	heap.Push(pq.pq, item)
	if item.Priority > pq.maxPrio {
		pq.maxPrio = item.Priority
	}
}

func (pq *PriorityQueueWithMutex) Dequeue() *Item {
	pq.mutex.Lock()
	defer pq.mutex.Unlock()

	if pq.pq.Len() == 0 {
		return nil
	}

	item := heap.Pop(pq.pq).(*Item)

	return item
}
func (pq *PriorityQueueWithMutex) Dequeue_unlock() *Item {
	if pq.pq.Len() == 0 {
		return nil
	}

	item := heap.Pop(pq.pq).(*Item)

	return item
}

func (pq *PriorityQueueWithMutex) MaxPriority() float64 {
	pq.mutex.Lock()
	defer pq.mutex.Unlock()

	return pq.maxPrio
}
func (pq *PriorityQueueWithMutex) MaxPriority_unlock() float64 {
	return pq.maxPrio
}

func (pq *PriorityQueueWithMutex) Update(cnt int, nodeName string, priority float64, final bool) {
	pq.mutex.Lock()
	defer pq.mutex.Unlock()

	for idx, item := range *pq.pq {
		if item.Cnt == cnt && item.NodeName == nodeName {
			item.Priority = priority
			item.Final = final
			if (item.Priority>pq.maxPrio){
				pq.maxPrio=priority
			}
			heap.Fix(pq.pq, idx)
			break
		}
	}
}
func (pq *PriorityQueueWithMutex) Update_unlock(cnt int, nodeName string, priority float64, final bool) {
	for idx, item := range *pq.pq {
		if item.Cnt == cnt && item.NodeName == nodeName {
			item.Priority = priority
			item.Final = final
			if (item.Priority>pq.maxPrio){
				pq.maxPrio=priority
			}
			heap.Fix(pq.pq, idx)
			break
		}
	}
}
func (pq *PriorityQueueWithMutex) Queue_init() {
	pq.pq = NewPriorityQueue()
	pq.maxPrio = 0
	pq.maxMsgCnt = 0
	pq.mutex = sync.Mutex{}
}
func (pq *PriorityQueueWithMutex) MinPriorityItem_unlock() *Item {
	if pq.pq.Len() == 0 {
		return nil
	}
	return (*pq.pq)[0]
}
func (pq *PriorityQueueWithMutex) queue_check_unlock(){
	item:=(*pq).MinPriorityItem_unlock()
	sentMsgIdx := item.NodeName + strconv.FormatInt(int64(item.Cnt),10)
	if sentMsgCnt[sentMsgIdx]&nodeIdxSum == nodeIdxSum {
		agreedPriorityMsg := "agreedPriority|" + item.NodeName + "|" + strconv.FormatInt(int64(item.Cnt),10) + "|" + strconv.FormatFloat(sentMsgPriorites[sentMsgIdx], 'f', -1, 64) + "|" + item.Content.(string) + "\n"
		waiter.Add(1)
		sendFinalPriority(sendConn, agreedPriorityMsg, &waiter)
		queue.Update_unlock(item.Cnt, item.NodeName, sentMsgPriorites[sentMsgIdx], true)
		receiveFinalPriority_unlock()
		delete(sentMsgCnt, sentMsgIdx)
		delete(sentMsgPriorites, sentMsgIdx)
	}

}
func (pq *PriorityQueueWithMutex) PrintQueue() {
	pq.mutex.Lock()
	defer pq.mutex.Unlock()

	fmt.Println("Queue Contents:")
	for i, item := range *pq.pq {
		fmt.Printf("Item %d: Content=%v, Priority=%f, Final=%t, NodeName=%s, Cnt=%d, Index=%d\n",
			i+1, item.Content, item.Priority, item.Final, item.NodeName, item.Cnt, item.Index)
	}
}
func (pq *PriorityQueueWithMutex) RemoveItemsByNodeName(nodeName string,deletedMsg map[string]map[string]bool) {
	pq.mutex.Lock()
	defer pq.mutex.Unlock()
	nodeIdxSum = nodeIdxSum - otherNodeId[nodeName]
	newPQ := make(PriorityQueue, 0)
	for _, item := range *pq.pq {
		if item.NodeName != nodeName || (item.NodeName == nodeName && item.Final == true) {
			newPQ = append(newPQ, item)
		}else{
			msgIdx := nodeName + strconv.FormatInt(int64(item.Cnt), 10)
			deletedMsg[nodeName][msgIdx] = true
		}
	}
	*pq.pq = newPQ
	heap.Init(pq.pq)
	pq.maxPrio = 0
	for _, item := range *pq.pq {
		if item.Priority > pq.maxPrio {
			pq.maxPrio = item.Priority
		}
	}
	firstMsgIdx := (*(queue.pq))[0].NodeName+strconv.FormatInt(int64((*(queue.pq))[0].Cnt), 10)
	if sentMsgCnt[firstMsgIdx]&nodeIdxSum == nodeIdxSum {
		agreedPriorityMsg := "agreedPriority|" + (*(queue.pq))[0].NodeName + "|" + strconv.FormatInt(int64((*(queue.pq))[0].Cnt), 10) + "|" + strconv.FormatFloat(sentMsgPriorites[firstMsgIdx], 'f', -1, 64) + "|" + (*(queue.pq))[0].Content.(string) + "\n"
		waiter.Add(1)
		sendFinalPriority(sendConn, agreedPriorityMsg, &waiter)
		queue.mutex.Lock()
		queue.Update_unlock((*(queue.pq))[0].Cnt, (*(queue.pq))[0].NodeName, sentMsgPriorites[firstMsgIdx], true)
		receiveFinalPriority_unlock()
		queue.mutex.Unlock()
		delete(sentMsgCnt, firstMsgIdx)
		delete(sentMsgPriorites, firstMsgIdx)
	}
}

var (
	nodeNum          int32 = 0
	nodeIdxSum		 int32 = 0
	nodeName         string
	nodeId           int32
	listenPort       int
	nb               int = 0
	deletedMsg		 map[string]map[string]bool	  = make(map[string]map[string]bool)
	waiter           sync.WaitGroup
	queue            PriorityQueueWithMutex
	otherNodeIP      map[string]string        = make(map[string]string)
	otherNodeId 	 map[string]int32         = make(map[string]int32)
	otherNodePort    map[string]int           = make(map[string]int)
	sendConn         map[string]net.Conn      = make(map[string]net.Conn)
	receiveConn      map[string]*bufio.Reader = make(map[string]*bufio.Reader)
	sentMsgPriorites map[string]float64       = make(map[string]float64)
	sentMsgCnt       map[string]int32         = make(map[string]int32)
)
var (
	account      map[string]int = make(map[string]int)
	account_lock sync.Mutex
)

func main() {
	if len(os.Args) != 3 {
		fmt.Printf("Arguments should follow the format: ./mp1node, identifier, configuration file\n")
		return
	}
	nodeName = os.Args[1]
	configFile := os.Args[2]
	readConfigFile(configFile)

	//metricfile
	metricfile, _ := os.OpenFile(nodeName+"generation", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	metricfile.WriteString("transactionid,time")
	metricfile.Close()
	metricfile, _ = os.OpenFile(nodeName+"deliver", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	metricfile.WriteString("transactionid,time")
	metricfile.Close()

	// Establish send connection
	waiter.Add(1)
	go establishSend(otherNodeIP, otherNodePort, &waiter)

	// Establish receive socket
	waiter.Add(1)
	go establishReceive(otherNodeIP, otherNodePort, &waiter)

	waiter.Wait()
	// fmt.Println("ok")
	for name, _ := range otherNodeIP {
		waiter.Add(1)
		// fmt.Printf("%s start to catch event from cmd line\n", name)
		go receiveMessage(name, &waiter)
	}
	queue.Queue_init()
	waiter.Add(1)
	go sendInitiation(&waiter)
	waiter.Wait()
	return
}

func readConfigFile(configFile string) {
	var flag bool = false
	var cnt int32 = 1
	file, err := os.Open(configFile)
	if err != nil {
		handleFuncError(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := strings.Fields(scanner.Text())
		if len(line) == 1 {
			nodeNumTemp, _ := strconv.Atoi(line[0])
			nodeNum = int32(nodeNumTemp)
		} else if len(line) == 3 {
			otherNodeName := string(line[0])
			if otherNodeName != nodeName {
				ip := line[1]
				port, _ := strconv.Atoi(line[2])
				otherNodeIP[otherNodeName] = ip
				otherNodePort[otherNodeName] = port
				otherNodeId[otherNodeName] = cnt
				nodeIdxSum = nodeIdxSum + cnt
				cnt = cnt<<1
			} else {
				nodeId = cnt
				nodeIdxSum = nodeIdxSum + cnt
				cnt = cnt<<1
				listenPort, _ = strconv.Atoi(line[2])
				flag = true
			}
		}
	}
	if !flag {
		handleFuncError("Invalid Identifier")
	}
	// fmt.Println("finish reading config file")
	return
}

func establishSend(ip map[string]string, port map[string]int, waiter *sync.WaitGroup) {
	defer waiter.Done()
	for nodeId, _ := range ip {
		var con net.Conn
		var err error
		for {
			con, err = net.Dial("tcp", fmt.Sprintf("%s:%d", ip[nodeId], port[nodeId]))
			if err == nil {
				break
			}
		}
		sendConn[nodeId] = con
		// fmt.Printf("%s to %s send connection established\n", nodeName, nodeId)
		con.Write([]byte(nodeName + ";"))
	}
	// fmt.Println("establishSend finished")
}

func establishReceive(ip map[string]string, port map[string]int, waiter *sync.WaitGroup) {
	defer waiter.Done()
	var cnt int32 = 0
	listener, err := net.Listen("tcp", ":"+strconv.FormatInt(int64(listenPort), 10))
	if err != nil {
		handleFuncError(err)
	}
	defer listener.Close()
	for cnt < nodeNum-1 {
		con, err := listener.Accept()
		if err != nil {
			handleFuncError(err)
		}
		reader := bufio.NewReader(con)
		otherNodeName, err := reader.ReadString(';')
		if err != nil {
			handleFuncError(err)
		}
		// fmt.Println(otherNodeName)
		otherNodeName = strings.TrimRight(otherNodeName, ";")
		receiveConn[otherNodeName] = reader

		cnt++
	}
	// fmt.Println("establishReceive finished")
}

func sendInitiation(waiter *sync.WaitGroup) {
	defer waiter.Done()
	reader := bufio.NewReader(os.Stdin)
	for {
		input, err := reader.ReadString('\n')
		if err != nil {
			handleFuncError(err)
		}
		queue.mutex.Lock()
		queue.maxMsgCnt++
		sentMsgIdx := nodeName + strconv.FormatInt(int64(queue.maxMsgCnt), 10)
		msg := "trivial|" + nodeName + "|" + strconv.FormatInt(int64(queue.maxMsgCnt), 10) + "|0|" + input
		queue.mutex.Unlock()

		//genetaion
		metricfile, _ := os.OpenFile(nodeName+"generation", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		gentime := gettime(time.Now())
		metricfile.WriteString(fmt.Sprintf("%s,%s\n", nodeName+strconv.FormatInt(int64(queue.maxMsgCnt), 10), strconv.FormatFloat(gentime, 'f', 7, 64)))
		metricfile.Close()
		waiter.Add(1)
		queue.mutex.Lock()
		maxPriority := queue.MaxPriority_unlock()
		sentMsgPriorites[sentMsgIdx] = math.Round(maxPriority+1) + float64(nodeId)/10
		queue.Enqueue_unlock(&Item{Content: input[:len(input)-1], Priority: sentMsgPriorites[sentMsgIdx], Final: false, NodeName: nodeName, Cnt: queue.maxMsgCnt})
		queue.mutex.Unlock()
		sentMsgCnt[sentMsgIdx] = nodeId
		bMulticast(sendConn, msg, waiter)


	}
}

func preprocessMsg(msg string) (string, string, string, string, string) {
	segments := strings.Split(msg, "|")
	msgType := segments[0]
	msgNodeName := segments[1]
	msgCnt := segments[2]
	msgPriority := segments[3]
	msgContent := segments[4]
	return msgType, msgNodeName, msgCnt, msgPriority, msgContent
}

func receiveMessage(name string, waiter *sync.WaitGroup) {
	defer waiter.Done()
	conn := receiveConn[name]
	receiveScanner := bufio.NewScanner(conn)
	if receiveScanner == nil {
		handleNodeFailure(name)
		return
	}
	if err := receiveScanner.Err(); err != nil {
		handleNodeFailure(name)
		return
	}

	for receiveScanner.Scan() {
		message := receiveScanner.Text()
		// fmt.Printf("%s\n", message)


		msgType, msgNodeName, msgCnt, msgPriority, msgContent := preprocessMsg(message)
		msgIdx := msgNodeName+msgCnt
		msgCntInt, _ := strconv.Atoi(msgCnt)
		msgPriorityFloat64, _ := strconv.ParseFloat(msgPriority, 64)
		switch msgType {
		case "proposedPriority":
			sentMsgIdx := msgNodeName + msgCnt
			sentMsgCnt[sentMsgIdx] = sentMsgCnt[sentMsgIdx]+otherNodeId[name]
			sentMsgPriorites[sentMsgIdx] = math.Max(sentMsgPriorites[sentMsgIdx], msgPriorityFloat64)
			// queue.maxPrio = math.Max(sentMsgPriorites[sentMsgIdx], queue.maxPrio)
			if sentMsgCnt[sentMsgIdx]&nodeIdxSum == nodeIdxSum {
				agreedPriorityMsg := "agreedPriority|" + msgNodeName + "|" + msgCnt + "|" + strconv.FormatFloat(sentMsgPriorites[sentMsgIdx], 'f', -1, 64) + "|" + msgContent + "\n"
				waiter.Add(1)
				sendFinalPriority(sendConn, agreedPriorityMsg, waiter)
				queue.mutex.Lock()
				queue.Update_unlock(msgCntInt, msgNodeName, sentMsgPriorites[sentMsgIdx], true)
				receiveFinalPriority_unlock()
				queue.mutex.Unlock()
				delete(sentMsgCnt, sentMsgIdx)
				delete(sentMsgPriorites, sentMsgIdx)
			}
		case "agreedPriority":
			queue.mutex.Lock()
			queue.queue_check_unlock()


			var ok1,ok2 bool = false,false
			_,ok1 = deletedMsg[msgNodeName]
			if ok1 {
				_,ok2 = deletedMsg[msgNodeName][msgIdx]
			}
			for _, item := range *(queue.pq) {
				if item.NodeName == msgNodeName && item.Cnt == msgCntInt && item.Final == false {
					queue.Update_unlock(msgCntInt, msgNodeName, msgPriorityFloat64, true)
					receiveFinalPriority_unlock()
					waiter.Add(1)
					bMulticast(sendConn, message+"\n", waiter)
					break
				}else if ok1&&ok2 {
					queue.Update_unlock(msgCntInt, msgNodeName, msgPriorityFloat64, true)
					receiveFinalPriority_unlock()
					waiter.Add(1)
					bMulticast(sendConn, message+"\n", waiter)
					delete(deletedMsg[msgNodeName], msgIdx)
					break
				}
			}
			// queue.Update_unlock(msgCntInt, msgNodeName, msgPriorityFloat64, true)
			// receiveFinalPriority_unlock()
			queue.mutex.Unlock()
		case "trivial":
			queue.mutex.Lock()
			queue.queue_check_unlock()


			maxPriority := queue.MaxPriority_unlock()
			proposedPriority := math.Round(maxPriority+1) + float64(nodeId)/10
			maxPriority++
			queue.Enqueue_unlock(&Item{Content: msgContent, Priority: proposedPriority, Final: false, NodeName: msgNodeName, Cnt: msgCntInt})
			queue.mutex.Unlock()
			proposedPriorityMsg := "proposedPriority|" + msgNodeName + "|" + msgCnt + "|" + strconv.FormatFloat(proposedPriority, 'f', -1, 64) + "|" + msgContent + "\n"
			sendConn[msgNodeName].Write([]byte(proposedPriorityMsg))
		default:
			fmt.Printf("%s received an invalid event\n", nodeName)
			os.Exit(1)
		}
		if receiveScanner == nil {
			handleNodeFailure(name)
			return
		}
		if err := receiveScanner.Err(); err != nil {
			handleNodeFailure(name)
			return
		}
	}
	// fmt.Printf("Error reading from connection: %s\n", name)
	handleNodeFailure(name)
}

func handleFuncError(err ...any) {
	// fmt.Println("Error:", err)
	os.Exit(1)
}

func handleNodeFailure(nodeName string) {
	sendConn[nodeName].Close()
	delete(sendConn, nodeName)
	delete(receiveConn, nodeName)
	deletedMsg[nodeName] = make(map[string]bool)
	queue.RemoveItemsByNodeName(nodeName,deletedMsg)
	// fmt.Printf("%s disconnected.\n", nodeName)
}

func bMulticast(sendCon map[string]net.Conn, message string, waiter *sync.WaitGroup) {
	defer waiter.Done()
	for _, nodeConn := range sendCon {
		nodeConn.Write([]byte(message))
	}
}
func receiveFinalPriority_unlock() {


	
	// fmt.Println("Queue Contents:")
	// i:=0
	// item:=(*(queue.pq))[i]

	// 	fmt.Printf("Item %d: Content=%v, Priority=%f, Final=%t, NodeName=%s, Cnt=%d, Index=%d\n",
	// 		i+1, item.Content, item.Priority, item.Final, item.NodeName, item.Cnt, item.Index)
	// 		i=i+1
	// 		item=(*(queue.pq))[i]

	// 		fmt.Printf("Item %d: Content=%v, Priority=%f, Final=%t, NodeName=%s, Cnt=%d, Index=%d\n",
	// 			i+1, item.Content, item.Priority, item.Final, item.NodeName, item.Cnt, item.Index)


	// queue.PrintQueue()
	item:= queue.MinPriorityItem_unlock()
	if item.Final == true {

		content := item.Content.(string)
		updateAccounts(content)
		item = queue.Dequeue_unlock()

		//log
		metricfile, _ := os.OpenFile(nodeName+"deliver", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		receivetime := gettime(time.Now())
		metricfile.WriteString(fmt.Sprintf("%s,%s\n", item.NodeName+strconv.FormatInt(int64(item.Cnt), 10), strconv.FormatFloat(receivetime, 'f', 7, 64)))
		metricfile.Close()
	}
}
func receiveFinalPriority() {

	// queue.PrintQueue()
	queue.mutex.Lock()
	item := queue.MinPriorityItem_unlock()
	if item.Final == true {

		content := item.Content.(string)
		updateAccounts(content)
		item = queue.Dequeue_unlock()

		//log
		metricfile, _ := os.OpenFile(nodeName+"deliver", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		receivetime := gettime(time.Now())
		metricfile.WriteString(fmt.Sprintf("%s,%s\n", item.NodeName+strconv.FormatInt(int64(item.Cnt), 10), strconv.FormatFloat(receivetime, 'f', 7, 64)))
		metricfile.Close()
	}
	queue.mutex.Unlock()
}

func updateAccounts(content string) {

	account_lock.Lock()
	parts := strings.Split(content, " ")
	content_type := parts[0]
	first_account_name := parts[1]
	if content_type == "DEPOSIT" {
		numberstring := parts[2]
		numberint, _ := strconv.Atoi(numberstring)
		if numberint > 0 {
			money, exist := account[first_account_name]
			if exist == true {
				new_money := money + numberint
				account[first_account_name] = new_money
			} else {
				account[first_account_name] = numberint
			}
		}
	} else if content_type == "TRANSFER" {
		second_account_name := parts[3]
		numberstring := parts[4]
		numberint, _ := strconv.Atoi(numberstring)
		firstmoney, firstexist := account[first_account_name]
		if numberint > 0 && (firstmoney >= numberint) && firstexist == true {
			secondmoney, secondexist := account[second_account_name]
			first_new_money := firstmoney - numberint
			account[first_account_name] = first_new_money
			if first_new_money == 0 {
				delete(account, first_account_name)
			}
			if secondexist == true {
				second_new_money := secondmoney + numberint
				account[second_account_name] = second_new_money
			} else {
				account[second_account_name] = numberint
			}
		}
	} else {
		// fmt.Println("The transaction type is wrong")
	}
	printBalances()
	account_lock.Unlock()
}
func printBalances() {
	// fmt.Printf("%d ",nb)
	nb++
	fmt.Printf("BALANCES")
	var names []string
	for name := range account {
		names = append(names, name)
	}
	sort.Strings(names)
	for _, name := range names {
		fmt.Printf(" %s:%d", name, account[name])
	}
	fmt.Printf("\n")
}
func sendFinalPriority(sendConn map[string]net.Conn, agreedPriorityMsg string, waiter *sync.WaitGroup) {
	defer waiter.Done()
	waiter.Add(1)
	bMulticast(sendConn, agreedPriorityMsg, waiter)
}

func gettime(thistime time.Time) float64 {
	now := thistime
	seconds := now.Unix()
	nanoSeconds := now.Nanosecond()
	decimalSeconds := float64(nanoSeconds) / 1e9
	totalSeconds := float64(seconds) + decimalSeconds
	return totalSeconds
}
